var express = require('express');
var Department = require('./Department');
var router = express.Router();
const Product = require('./Product');

router.post('/', function(req, res){
    let d = new Department({name: req.body.name});
    d.save((err, dep) => {
        if (err) 
            res.status(500).send(err);
        else
            res.status(200).send(dep);

    });
});

router.get('/', function(req, res){
   
    Department.find().exec((err, deps) => {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(deps);

    });
});

router.delete('/:id', async (req, res) => {
    try {
            let id = req.params.id;
            let  prods =  await Product.find({departments: id}).exec();
            if(prods.lengh >0 ){
                res.status(500).send({
                    msg: 'Departamento não pode ser removido, resolva as dependencias antes de apagar'
                 
                    })
                } else {
                    await Department.deleteOne({_id: id});
                    res.status(200).send({});
                    }
        
        } catch (err) {
        res.status(500).send({msg: 'Erro Interno.', error: err})
    
        }
        
});

router.patch('/:id', (req, res) => {
    Department.findById(req.params.id, (err, dep) => {
        if (err)
            res.status(500).send(err)
        else if (!dep)
            res.status(404).send('o erro ta aqui');
        else {
            dep.name = req.body.name;
            dep.save()
            .then((d) => res.status(200).send(d))
            .catch((e) => res.status(500).send(e));
        }
    });
});


module.exports = router
